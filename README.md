# Find broken Youtube bookmarks
## Requirements
* Python 3 (<https://www.python.org/>)
* Youtube API key (<https://console.developers.google.com/apis/>)

## Installation
1. Clone repository and navigate to folder:
```bash
git clone https://gitlab.com/miksuh/find-unavailable-youtube-bookmarks.git
cd find-unavailable-youtube-bookmarks
```
2. Install dependencies
```bash
pip3 install BeautifulSoup4 requests
```
3. Set your API key into `main.py`
4. Export your browsers bookmarks, save into project folder as bookmarks.html

## Running
```bash
python3 ./main.py
```
Script only prints videos not available and you can easily search those urls in bookmarks and remove them.