from bs4 import BeautifulSoup
import requests

soup = BeautifulSoup(open("./bookmarks.html"), "html.parser")

your_api_key = "API_KEY_HERE"

def main():
    for link in soup.findAll("a"):
        href = link.get("href")
        if href.startswith("https://www.youtube.com/watch?v="):

            splitted = href.split("=")

            if len(splitted) == 2:
                id = splitted[1]
            else:
                id = splitted[1].split("&")[0]

            url = f'https://www.googleapis.com/youtube/v3/videos?id={id}&key={your_api_key}&part=status'
            url_get = requests.get(url)

            try:
                if url_get.json()['pageInfo']['totalResults'] == 0:
                    print(href)
            except KeyError:
                pass


if __name__ == "__main__":
    main()
